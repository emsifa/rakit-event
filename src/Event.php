<?php

namespace Rakit\Event;

class Event {

    protected $listeners = array();

    public function on($event, $callback, $limit_called = 0)
    {
        $this->register($event, $callback, $limit_called, $this->listeners);
        return $this;
    }

    public function fire($event)
    {
        $args = func_get_args();
        
        $count_running = 0;

        if("*" == substr($event, -1, 1)) {
            $regex_evtname = str_replace('.', "\\.", $event);
            $regex_evtname = '/^'.preg_replace('/\*$/', '(([a-zA-Z0-9_ -]+|\*)\.?)+', $regex_evtname).'$/';

            foreach($this->listeners as $evt) {
                $regex_evt = str_replace('.', "\\.", $evt['name']);
                $regex_evt = '/^'.preg_replace('/\*$/', '(([a-zA-Z0-9_ -]+|\*)\.?)+', $regex_evt).'$/';

                if(preg_match($regex_evtname, $evt['name']) OR preg_match($regex_evt, $event)) {
                    call_user_func_array($evt['callback'], $args);
                    $count_running++;

                    $evt['called']++;

                    if($evt['called'] == $evt['limit']) {
                        $index = array_search($evt, $this->listeners);
                        unset($this->listeners[$index]);       
                    }
                }
            }
        } else {
            $event_names = $this->getEventNames($event);

            foreach($this->listeners as $evt) {
                if(in_array($evt['name'], $event_names)) {
                    call_user_func_array($evt['callback'], $args);
                    $count_running++;

                    $evt['called']++;

                    if($evt['called'] == $evt['limit']) {
                        $index = array_search($evt, $this->listeners);
                        unset($this->listeners[$index]);       
                    }
                }
            }   
        }
        
        return $count_running;
    }

    protected function register($event, $callback, $limit_called = 0, array &$listeners = array())
    {
        $callback = $this->makeCallable($callback);
        if(!is_callable($callback)) {
            throw new \Exception("Callback is not callable");
        }

        $listeners[] = array(
            'name' => $event,
            'limit' => $limit_called,
            'called' => 0,
            'callback' => $callback
        );
    }

    protected function getEventNames($event)
    {
        $event_names = array();
        $expl = explode('.', $event);

        $evt = array();
        $evts = array();
    
        foreach($expl as $key) {
            $evt_name = ltrim(implode('.', $evt).'.*', '.');
            $event_names[] = $evt_name;
            $evt[] = $key;
        }
    
        $event_names[] = $event;
    
        return array_reverse($event_names);
    }

    protected function makeCallable($callback)
    {
        return $callback;
    }

}