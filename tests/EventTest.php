<?php

use Rakit\Event\Event;

class EventTest extends PHPUnit_Framework_TestCase {

    public function testEvent()
    {
        $event = new Event;

        $event->on("foo.onetime", function() {
            echo "onetime";
        }, 1);

        $event->on("foo", function() {
            echo "1";
        });

        $event->on("foo.bar", function() {
            echo "2";
        });

        $event->on("foo.bar.baz", function() {
            echo "3";
        });

        $event->on("*", function() {
            echo "4";
        });

        $event->on("foo.*", function() {
            echo "5";
        });

        $event->on("foo.bar.*", function() {
            echo "6";
        });

        $this->assertEquals("onetime123456", $this->output(function() use ($event) {
            return $event->fire("*");
        }));

        $this->assertEquals("23456", $this->output(function() use ($event) {
            return $event->fire("foo.*");
        }));

        $this->assertEquals("3456", $this->output(function() use ($event) {
            return $event->fire("foo.bar.*");
        }));

        $this->assertEquals("456", $this->output(function() use ($event) {
            return $event->fire("foo.bar.baz.*");
        }));

        $this->assertEquals("14", $this->output(function() use ($event) {
            return $event->fire("foo");
        }));

        $this->assertEquals("245", $this->output(function() use ($event) {
            return $event->fire("foo.bar");
        }));

        $this->assertEquals("3456", $this->output(function() use ($event) {
            return $event->fire("foo.bar.baz");
        }));

        $this->assertEquals("456", $this->output(function() use ($event) {
            return $event->fire("foo.bar.baz.qux");
        }));        

        $this->assertEquals("4", $this->output(function() use ($event) {
            return $event->fire("baz");
        }));

    }

    protected function output(\Closure $callback)
    {
        ob_start();
        $c = $callback();
        $buffer = ob_get_contents();
        ob_end_clean();

        //echo ">>> {$c} '".$buffer."'\n";

        return $buffer;
    }
    
}